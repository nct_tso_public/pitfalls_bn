# Official implementation of MedIA paper 'On the Pitfalls of Batch Normalization for End-to-End Video Learning: A Study on Surgical Workflow Analysis'

This [MedIA publication](https://www.sciencedirect.com/science/article/pii/S1361841524000513) investigates the pitfalls of Batch Normalization ([graphical abstract](graphicalabstract_pitfalls_bn.pdf)) for two Surgical Workflow tasks (phase recognition and instrument anticipation). This code base provides an extensive framework for training phase and anticipation models with different batch compositions, backbones, temporal heads, and training strategies.

- **Tasks**
  - Surgical Phase Recognition ([original paper](https://arxiv.org/abs/1602.03012))
  - Anticipation of Instrument Usage ([original paper](https://arxiv.org/abs/2007.00548))
- **Backbones**
  - ResNet50 w/ BN, GN, FrozenBN
  - ConvNeXt
  - AlexNet
  - + many more which did not make it into the paper (e.g. NFNet, VGG, ResNet18/34, ...)
- **Temporal Heads**
  - MS-TCN
  - LSTM
- **Training Strategies**
  - End-to-end training of backbone and temporal model
  - Partially frozen backbone (but still only 1 training stage)
  - 2-stage training (first backbone, then temporal model)
- **Batch Compositions**
  - N sequences of T frames (for end-to-end learning)
  - Complete video (for 2nd stage of 2-stage methods)
- **LSTM strategies for end-to-end training**
	- "SWE": reset hidden state for each training sequence, evaluate with sliding window
	- "CHE": reset hidden state for each training sequence, carry hidden state through entire video during evaluation
	- "CHT" (proposed): carry hidden state through video during training and evaluation

## Preparation

### Step 1:

<details>
<summary>Download the Cholec80 dataset</summary>

- Access can be requested [here](http://camma.u-strasbg.fr/datasets)
- Download the 80 videos and extract frames at 1fps. E.g. for `video01.mp4` with ffmpeg, run:
```bash
mkdir /<PATH_TO_THIS_FOLDER>/data/frames_1fps/01/
ffmpeg -hide_banner -i /<PATH_TO_VIDEOS>/video01.mp4 -r 1 -start_number 0 /<PATH_TO_THIS_FOLDER>/data/frames_1fps/01/%08d.jpg
```
- The final dataset structure should look like this:

```
data/
	frames_1fps/
		01/
			00000001.jpg
			00000002.jpg
			00000003.jpg
			00000004.jpg
			...
		02/
			...
		...
		80/
			...
	phase_annotations/
		video01-phase.txt
		video02-phase.txt
		...
		video80-phase.txt
	tool_annotations/
		video01-tool.txt
		video02-tool.txt
		...
		video80-tool.txt
```

</details>

### Step 2: 

<details>
<summary>Download pretrained models (ResNet50 w/ GroupNorm, and ConvNeXt-T)</summary>

- download ResNet50 w/ GN [weights](https://github.com/ppwwyyxx/GroupNorm-reproduce/releases/download/v0.1/ImageNet-ResNet50-GN.pth) and place here: `train_scripts/group_norm/ImageNet-ResNet50-GN.pth`

- download ConvNeXt-T [weights](https://dl.fbaipublicfiles.com/convnext/convnext_tiny_1k_224_ema.pth) and place here: `train_scripts/convnext/convnext_tiny_1k_224_ema.pth`

</details>

## Requirements

See [requirements.txt](requirements.txt).

## Checkpoints

### Phase Recognition:
Partially frozen CNN-LSTM, 1x256, CHT, 32/8/40 split (SOTA comparison)

| Backbone | Run | Relaxed Acc. | Relaxed Jacc. | Acc. | Jacc. | Balanced Acc. | Checkpoint |
| -------- | ------- | ------- | ------- | ------- | ------- | ------- | ------- |
| ConvNext    | Run 1 | 93.7 | 82.6 | 92.7 | 76.9 | 88.4 | [weights](https://caruscloud.uniklinikum-dresden.de/index.php/s/zrbPfdBX9H3Dszf) |
|             | Run 2 | 93.4 | 82.5 | 92.1 | 75.9 | 86.6 | [weights](https://caruscloud.uniklinikum-dresden.de/index.php/s/5M4bdctowsfFg2f) |
|             | Run 3 | 93.5 | 83.7 | 92.3 | 77.8 | 88.9 | [weights](https://caruscloud.uniklinikum-dresden.de/index.php/s/22BqAW2QLGeKPD4) |
| ResNet50-GN | Run 1 | 93.2 | 79.7 | 92.2 | 73.8 | 86.2 | [weights](https://caruscloud.uniklinikum-dresden.de/index.php/s/anJCneP5fskFHTg) |
|             | Run 2 | 92.2 | 78.7 | 91.2 | 72.1 | 85.6 | [weights](https://caruscloud.uniklinikum-dresden.de/index.php/s/X46DpZQFqZyxTeZ) |
|             | Run 3 | 93.8 | 82.1 | 92.9 | 75.8 | 87.4 | [weights](https://caruscloud.uniklinikum-dresden.de/index.php/s/MpF7RSwg9QqmEtT) |

## Inference & Evaluation:

Download zipped checkpoints, extract and place folder into `output/checkpoints/phase/`.\
Checkpoint file should now be at `output/checkpoints/phase/[TRIAL_NAME]/models/checkpoint_best_acc.pth.tar`.

_Accuracy & Balanced Accuracy:_

```bash
python3 save_predictions.py phase --split cuhk --backbone [convnext,resnet50_gn] --seq_len 256 --resume output/checkpoints/phase/[TRIAL_NAME]/models/checkpoint_best_acc.pth.tar
```

Prediction files and scores will be written to `output/predicitons/phase/[TRIAL_NAME]`.

_Relaxed Accuracy & Relaxed Jaccard:_

In the matlab script `../matlab-eval/Main.m`, set:
```matlab
line 8: gt_root_folder = '../output/predictions/phase/[TRIAL_NAME]/gt/';
line 23: predroot = '../output/predictions/phase/[TRIAL_NAME]/pred/';
```

Then run `Main.m` in matlab or octave.

## Training: Quick Start

### Proposed configuration (phase recognition):

_Partially frozen CNN-LSTM, 1x256 batches, CHT (32/8/40 data split for SOTA comparison):_

```bash
python3 train.py phase --split cuhk --backbone [convnext,resnet50_gn] --freeze --seq_len 256 --lr 1e-4 --random_seed --trial_name [TRIAL_NAME]
```

## Training: General Usage

### Parameters

Below are the most important parameters to execute the different training strategies analyzed in the paper. See [train_scripts/options_train.py](train_scripts/options_train.py) for the complete list of options and their default values.

```bash
python3 train.py
	[phase,anticipation]
	--split [cuhk,tecno]    # cuhk=32/8/40, tecno=40/8/32
	--backbone [convnext,resnet50_gn,resnet50,...]
	[--bn_off]              # FrozenBN. only in combination with "--backbone resnet50"
	--head [lstm,tcn]
	--batch_size [N]
	--seq_len [T]
	[--shuffle]             # training sequences are sampled randomly.
				# if not set, sequences are sampled in temporal order and batch_size should be 1
	[--sliding_window]      # uses LSTM with a sliding window of "seq_len" at inference.
				# if not set, hidden state is carried through the whole surgery during inference.
	[--freeze]              # freezes bottom 3 backbone layers
	[--image_based]         # trains backbone only. (+ shorter epochs, more frequent checkpoints)
	[--only_temporal]       # trains temporal models only on complete surgeries
				# ("--seq_len" has no effect if this is set)
				# (use only with "--batch_size 1" and WITHOUT "--shuffle")
	--lr [LR]
	--epochs [EPOCHS]
	--random_seed
	--trial_name [TRIAL_NAME]
```

Since there are several combinations of parameters which do not lead to meaningful configurations (e.g. `--batch_size >1` without `--shuffle`, or `--only_temporal` with `--shuffle`), see examples below for proper usage of the different training strategies.

### Example commands

#### Phase recognition (40/8/32 split)

This is the data split used for the main analysis (40/8/32), *not* the state-of-the-art comparison. For the SOTA split (32/8/40), simply add `--split cuhk` to each command.

<details>
<summary>Show examples</summary>

<table>
<tr>
<td><b>Training Strategy</b></td><td><b>Details</b></td><td><b>Command</b></td>
</tr>

<tr>
<td>End-to-end</td><td>1x64, CHT</td>
<td>

```bash
python3 train.py phase --backbone [resnet50_gn,convnext] --random_seed
```

</td>
</tr>
<tr>
<td></td><td>1x64, CHT<br>w/ FrozenBN</td>
<td>

```bash
python3 train.py phase --backbone resnet50 --bn_off --lr 1e-6 --random_seed
```

</td>
</tr>
<tr>
<td></td><td>4x16, CHE</td>
<td>

```bash
python3 train.py phase --backbone resnet50 --batch_size 4 --seq_len 16 --shuffle --random_seed
```

</td>
</tr>
<tr>
<td></td><td>4x16, SWE</td>
<td>

```bash
python3 train.py phase --backbone resnet50 --batch_size 4 --seq_len 16 --shuffle --sliding_window --random_seed
```

</td>
</tr>

<tr>
<td>Partial freezing</td><td>1x256, CHT</td>
<td>

```bash
python3 train.py phase --backbone [resnet50_gn,convnext] --freeze --seq_len 256 --lr 1e-4 --epochs 100 --random_seed
```

</td>
</tr>
<tr>
<td></td><td>1x256, CHT<br>w/ FrozenBN</td>
<td>

```bash
python3 train.py phase --backbone resnet50 --bn_off --freeze --seq_len 256 --lr 1e-5 --epochs 100 --random_seed
```

</td>
</tr>
<tr>
<td></td><td>4x64, CHE</td>
<td>

```bash
python3 train.py phase --backbone resnet50 --freeze --batch_size 4 --seq_len 64 --lr 1e-4 --shuffle --epochs 100 --random_seed
```

</td>
</tr>

<tr>
<td>2-stage</td><td>Stage 1:<br>backbone only</td>
<td>

```bash
python3 train.py phase --backbone [resnet50,resnet50_gn,convnext] --image_based --batch_size 32 --seq_len 1 --shuffle --epochs 50 --random_seed
```

</td>
</tr>
<tr>
<td></td><td>Stage 2:<br>temporal head only</td>
<td>

```bash
python3 train.py phase --backbone [resnet50,resnet50_gn,convnext] --head [lstm,tcn] --only_temporal --lr 5e-4 --epochs 100 --cnn_weight_path [PATH_TO_MODEL]/checkpoint_best_acc.pth.tar --random_seed
```

</td>
</tr>

</table>

</details>

### Anticipation (40/8/32 split)

Commands are almost identical for anticipation, but it is recommended to train for more epochs due to slower convergence.\
Further, the anticipation horizon has to be specified (5 min in the paper).

<details>
<summary>Show examples</summary>

<table>
<tr>
<td><b>Training Strategy</b></td><td><b>Details</b></td><td><b>Command</b></td>
</tr>

<tr>
<td>End-to-end</td><td>1x64, CHE</td>
<td>

```bash
python3 train.py anticipation --horizon 5 --backbone [resnet50_gn,convnext] --shuffle --epochs 300 --random_seed
```

</td>
</tr>
<tr>
<td>Partial freezing</td><td>"Cheating" eval.<br>(using future info through BN)</td>
<td>

```bash
python3 train.py anticipation --horizon 5 --backbone resnet50 --shuffle --freeze --seq_len 256 --lr 1e-4 --epochs 300 --cheat --random_seed
```

</td>
</tr>
<tr>
<td>2-stage</td><td>Stage 1:<br>backbone only</td>
<td>

```bash
python3 train.py anticipation --horizon 5 --backbone [resnet50,resnet50_gn,convnext] --image_based --batch_size 32 --seq_len 1 --shuffle --epochs 300 --random_seed
```

</td>
</tr>
<tr>
<td></td><td>Stage 2:<br>temporal head only</td>
<td>

```bash
python3 train.py anticipation --horizon 5 --backbone [resnet50,resnet50_gn,convnext] --head [lstm,tcn] --only_temporal --lr 5e-4 --epochs 100 --cnn_weight_path [PATH_TO_MODEL]/checkpoint_best_wMAE.pth.tar --random_seed
```

</td>
</tr>

</table>

</details>

## Citation

If you use this code, please cite our paper:

```bibtex
@article{rivoir2024pitfalls,
  title={On the pitfalls of batch normalization for end-to-end video learning: A study on surgical workflow analysis},
  author={Rivoir, Dominik and Funke, Isabel and Speidel, Stefanie},
  journal={Medical Image Analysis},
  pages={103126},
  year={2024},
  publisher={Elsevier}
}
```

This work was carried out at the National Center for Tumor Diseases (NCT) Dresden, [Department of Translational Surgical Oncology](https://www.nct-dresden.de/tso.html) and the Centre for Tactile Internet ([CeTI](https://ceti.one/)) at TU Dresden.

## Acknowledgements

Funded by the German Research Foundation (DFG, Deutsche Forschungsgemeinschaft) as part of Germany’s Excellence Strategy – EXC 2050/1 – Project ID 390696704 – Cluster of Excellence “Centre for Tactile Internet with Human-in-the-Loop” (CeTI) of Technische Universität Dresden.

## License

Licensed under the CC BY-NC-SA 4.0 license (https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode). 

These licenses allow you to use, modify and share the project for non-commercial use as long as you adhere to the conditions of the license above.

## Contact

If you have any questions, do not hesitate to contact us: ```dominik.rivoir [at] nct-dresden.de```
